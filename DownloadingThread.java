package adv.downloader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

/**
 * Выполняет загрузку файла по указанной ссылке в указанное место на диске
 */
public class DownloadingThread extends Thread {

    private String link;
    private String file;

    /**
     * Инициализирует объект
     *
     * @param link ссылка на файл
     * @param file путь к файлу
     */
    public DownloadingThread(String link, String file) {
        this.link = link;
        this.file = file;
    }

    public void run() {
        try {
            URL musicURL = new URL(link);
            try(ReadableByteChannel byteChannel = Channels.newChannel(musicURL.openStream());
                FileOutputStream fileStream = new FileOutputStream(file)) {
                FileChannel fileChannel = fileStream.getChannel();
                fileChannel.transferFrom(byteChannel, 0, Long.MAX_VALUE);
            }

            System.out.println("Файл " + file + " загружен");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
